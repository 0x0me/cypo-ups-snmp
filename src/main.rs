use snmp::{SyncSession, Value};
use std::time::Duration;

struct OID<'a> {
    oid: &'a [u32],
    name: &'a str,
}

impl<'a> OID<'a> {
    fn new(id: &'a [u32], name: &'a str) -> OID<'a> {
        OID {
            oid: id,
            name: name,
        }
    }

    fn value(&self) -> &[u32] {
        self.oid
    }

    fn name(&self) -> &'a str {
        self.name
    }
}

fn main() {
    let cypo_ups_ident_model = OID::new(&[1, 3, 6, 1, 4, 1, 3808, 1, 1, 1, 1, 1, 1], "Model");
    let cypo_ups_ident_name = OID::new(&[1, 3, 6, 1, 4, 1, 3808, 1, 1, 1, 1, 1, 2], "Name");
    let cypo_ups_ident_firmware_revision = OID::new(
        &[1, 3, 6, 1, 4, 1, 3808, 1, 1, 1, 1, 2, 1],
        "Firmware Revision",
    );
    let cypo_ups_ident_serial_number =
        OID::new(&[1, 3, 6, 1, 4, 1, 3808, 1, 1, 1, 1, 2, 3], "Serial Number");
    let cypo_ups_output_load = OID::new(&[1, 3, 6, 1, 4, 1, 3808, 1, 1, 1, 4, 2, 3], "Load");

    let agent_addr = "127.0.0.1:161";
    let community = b"public";
    let timeout = Duration::from_secs(2);

    let mut sess = SyncSession::new(agent_addr, community, Some(timeout), 0).unwrap();
    // let mut response = sess.getnext(sys_descr_oid).unwrap();
    // if let Some((_oid, Value::OctetString(sys_descr))) = response.varbinds.next() {
    //      println!("myrouter sysDescr: {}", String::from_utf8_lossy(sys_descr));
    // }

    let ids = vec![
        cypo_ups_ident_model,
        cypo_ups_ident_name,
        cypo_ups_ident_firmware_revision,
        cypo_ups_ident_serial_number,
        cypo_ups_output_load,
    ];
    for id in ids {
        let mut response = sess.getnext(id.value()).unwrap();

        let r = response.varbinds.next();
        println!("==> {:?}", r);

        if let Some((oid, Value::OctetString(sys_descr))) =  r {
            println!(
                "{}[{}]: {}",
                id.name(),
                oid,
                String::from_utf8_lossy(sys_descr)
            );
        }

       

    }
}
